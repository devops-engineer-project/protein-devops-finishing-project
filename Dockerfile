# image with the smallest possible size alpine
FROM node:lts-alpine3.16
WORKDIR /usr/src/node_app
COPY create-react-app/ .
RUN npm install -g npm@8.13.2
EXPOSE 3000
CMD ["npm", "App.js"]
