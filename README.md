# Protein DevOps Engineer Finish Project :rocket:
## Used technologies;
### ![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white) ![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) ![Kubernetes](https://img.shields.io/badge/kubernetes-%23326ce5.svg?style=for-the-badge&logo=kubernetes&logoColor=white) ![GitLab CI](https://img.shields.io/badge/gitlab%20ci-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)
### Our mission is to fully cover the DevOps cycle and build a complete CI/CD and cloud infrastructure.

## ASSIGNMENT :closed_book:
- [ ] **Create a basic React.js app by following the . You can leave the application with its default settings.** [Facebook’s documentation](https://github.com/facebook/create-react-app) 
- [ ] **Create a [Dockerfile](Dockerfile) for your app. Try to form an image with the smallest possible size.**
- [ ] **Design and write a [GitLab CI/CD pipeline](.gitlab-ci.yml) featuring your Dockerfile for your brand new app.** 
---
### DEPLOY APPLICATION TO AWS ![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white)
- [ ] **Use **Terraform** to provision the infrastructure.**
- [ ] **Application should be running on [ECS Fargate](main.tf).**
- [ ] **Make sure to use the ideal [VPC](variables.tf) and security group settings**
- [ ] **Application load balancer must be configured in front of the service.**
- [ ] **Configure [auto-scaling](auto-scaling.tf) onto your Fargate instance. Scale-up when CPU is above %50 and scale-down when CPU is below %20.**
- [ ] **Create the proper [Cloudwatch](cloudwatch.tf) dashboards and metrics for monitoring the performance of the application.**
- [ ] **Feel free to include or use any other AWS service.**
---
### **DEPLOY THE APPLICATION TO KUBERNETES** ![Kubernetes](https://img.shields.io/badge/kubernetes-%23326ce5.svg?style=for-the-badge&logo=kubernetes&logoColor=white)

- [ ] **Write the required core [manifest files](kubernetes).**
---
### **DISK USAGE SCRIPT (SHELL)** ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)

- [ ] **[Write a script](disk_usage_alert) ( Ansible & Shell ) that sends a notification by E-mail if the disk usage exceeds 90% in an operating system using any Linux distribution (You can use any Gmail address as the sender E-mail address)**
---
### AWS architecture drawing
- [ ] [AWS architecture drawing](amazon_draw.png), connections between configured services.
