resource "aws_autoscaling_group" "task" {
  min_size             = 1
  max_size             = 3
  desired_capacity     = 1
  launch_configuration = aws_launch_configuration.task.name
  vpc_zone_identifier  = module.vpc.public_subnets
}
resource "aws_autoscaling_attachment" "task" {
  autoscaling_group_name = aws_autoscaling_group.task.id
}
module "fargate" {
  source = "./"
  name   = "main"

  services = {
    api = {
      container_port = 3000
      cpu            = "256"
      memory         = "512"
      replicas       = 1

      auto_scaling_max_replicas = 2  // Will scale out up to 2 replicas
      auto_scaling_max_cpu_util = 50 // If Avg CPU Utilization reaches 60%, scale up operations gets triggered
      auto_scaling_min_cpu_util = 20 // If Avg CPU Utilization reaches 20%, scale  operations gets triggered
    }
  }
}