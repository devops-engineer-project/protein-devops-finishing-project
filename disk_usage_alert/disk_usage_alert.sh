#!/bin/sh

#Current partitions
CURRENT=$(df / | grep / | awk '{ print $5}'| sed 's/%//g' )

MAIL="merveipekci4@gmail.com"

# Which date 
DATE=$(date +"%d/%m/%Y)

# Disk usage limit 
LIMIT=90

if ["$CURRENT" -gt "$LIMIT" ]; then

	mailx -s 'Disk Space Alert' $MAIL << EOF

Partition usage alarm used $CURRENT% $DATE

EOF

fi

